# 使用C语言写第一段程序

1. hello world

物理地址0x50000020代表的是s3c2410的串口FIFO寄存器地址，简单地说，就是写向该地址的数据都将会通过串口发送给另一端。这段程序只是串行的将字符串“helloworld”依次送给串口FIFO寄存器

```
#define UFCON0	((volatile unsigned int *)(0x50000020))

void helloworld(void)
{
	const char *p="hello skyeye\n";
	while(*p){
		*UFCON0=*p++;
	};
	while(1); 
}
```
2. 编译链接

```
arm-none-eabi-gcc -nostdlib -O2 -g -c helloworld.c
arm-none-eabi-ld -e helloworld -Ttext 0x0 helloworld.o -o helloworld.elf
arm-none-eabi-objcopy -O binary helloworld.elf helloworld.bin
```

参数-Ttext的作用是指定该程序的运行基地址，将目标文件的hellworld函数链接到内存0x0的位置上，并且从此处执行。在ARM体系结构中，程序必须从内存零地址处开始运行。

3. 运行
```
skyeye -s s3c2410x.skyeye
```

4. 反汇编分析
```
arm-none-eabi-objdump -D -b binary helloworld.bin

helloworld.bin:     file format binary

Contents of section .data:
 0000 6830a0e3 0512a0e3 10209fe5 203081e5  h0....... .. 0..
 0010 0130f2e5 000053e3 fbffff1a feffffea  .0....S.........
 0020 24000000 68656c6c 6f20736b 79657965  $...hello skyeye
 0030 0a000000                             ....            

Disassembly of section .data:

00000000 <.data>:
   0:	e3a03068 	mov	r3, #104	; 0x68
   4:	e3a01205 	mov	r1, #1342177280	; 0x50000000
   8:	e59f2010 	ldr	r2, [pc, #16]	; 0x20
   c:	e5813020 	str	r3, [r1, #32]
  10:	e5f23001 	ldrb	r3, [r2, #1]!                //将r2中地址的数据加载到r3，并且r2地址+1
  14:	e3530000 	cmp	r3, #0
  18:	1afffffb 	bne	0xc
  1c:	eafffffe 	b	0x1c
```



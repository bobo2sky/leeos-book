#!/bin/bash

arm-none-eabi-gcc -nostdlib -O2 -c helloworld.c
arm-none-eabi-ld -T helloworld.lds helloworld.o -o helloworld.elf
arm-none-eabi-objcopy -O binary helloworld.elf helloworld.bin

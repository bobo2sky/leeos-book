# 用脚本链接目标文件

1. 概述

链接脚本就是程序链接时的参考文件，其目的是描述输入文件中各段应该怎样被映射到输出文件，以及程序运行时的内存布局。

2. 链接脚本 

```
ENTRY(helloworld)
SECTIONS
{
	. = 0x00000000;
	.text :{
	   *(.text)
	}
	. = ALIGN(32);
	.data :{
	   *(.data)
	}
	. = ALIGN(32);
	.bss :{
	   *(.bss)
	}
}
```

（1）点号（.）
点号在SECTIONS命令里被称为位置计数器。通俗地讲，它代表了当前位置，你可以对位置计数器赋值。

（2）输出段定义
各关键字代表了输出段的段名。.text关键字定义了该输出位置为代码段，花括号内部定义了代码段的具体内容，其中，星号（*）代表所有文件，*(.text)的意思是所有目标文件的代码段都将被链接到这一区域，我们也可以特别地指定某个目标文件出现在代码段的最前面。.data关键字定义了该输出位置为数据段，其格式和用法与.text关键字相同。.bss关键字定义了输出位置是bss段。

（3）ALIGN(N)：在代码2-2中并没有ALIGN关键字。但是在实际应用中，我们经常需要使用ALIGN产生对齐的代码或数据。很多体系结构对对齐的代码或数据有严格的要求，还有一些体系结构在处理对齐的数据或代码时效率更高，这都体现了ALIGN关键字的重要性。我们可以用某一数值取代ALIGN()括号中的N，同时对位置计数器赋值，如.=ALIGN(4)就表示位置计数器会向高地址方向取最近的4字节的整数倍。

（4）ENTRY命令，该命令等同于arm-elf-ld命令的参数-e，即显式地指定哪一个函数为程序的入口。

3. 使用脚本链接

```
arm-none-eabi-gcc -nostdlib -O2 -c helloworld.c
arm-none-eabi-ld -T helloworld.lds helloworld.o -o helloworld.elf
arm-none-eabi-objcopy -O binary helloworld.elf helloworld.bin
```
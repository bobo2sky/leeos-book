# 汇编和C的混合编程

1. 混编版helloworld

```
.arch armv4
.global _start

.equ REG_FIFO, 0x50000020

.text
.align 2

_start:
	ldr r0,=REG_FIFO
	adr r1,.L0
	bl helloworld
.L1:
	b .L1

.align 2
.L0:
	.ascii "2.4_helloworld\n\0"
```

```
int helloworld(unsigned int *addr, const char *p)
{
	while(*p) {
		*addr = *p++;
	};

	return 0;
}
```
2. 说明

代码2-6不是直接将helloworld字符串送给串口FIFO寄存器，而是通过bl指令调用helloworld函数，由该函数负责数据写入

bl指令我们没有接触过，它的功能与b指令几乎一致。唯一的不同是bl指令在运行时，能够自动地保存程序的返回地址。这样，在成功调用子程序并且运行结束时，就能够返回原先的位置继续运行。

helloworld函数是由C语言写成的。根据AAPCS的规定，当函数发生调用的时候，函数的参数会保存在ARM核心寄存器R0～R3中，如果参数多于4个，则剩下的参数会保存在堆栈中。因此，我们也会把寄存器R0～R3称为参数寄存器（argumentregister），用别名a1～a4代替。在代码2-6中，调用helloworld函数之前，我们分别对R0、R1两个寄存器赋值，这两个值就是helloworld函数的参数。

调用helloworld函数之前，我们分别对R0、R1两个寄存器赋值，这两个值就是helloworld函数的参数

3. 反汇编

```

```
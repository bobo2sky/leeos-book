#!/bin/bash

arm-none-eabi-gcc -nostdlib -O2 -c start.s
arm-none-eabi-gcc -nostdlib -O2 -c helloworld.c

arm-none-eabi-ld -e _start -Ttext 0x0 start.o helloworld.o -o helloworld.elf
arm-none-eabi-objcopy -O binary helloworld.elf helloworld.bin

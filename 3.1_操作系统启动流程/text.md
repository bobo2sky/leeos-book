## 操作系统启动流程

### 一、ARM的启动过程

在CPU上电的时候，ARM就会首先从地址零处开始运行，通常是bootloader在0地址启动，再加载操作系统到内存镜像指定地址，跳转过去执行

操作系统启动前要完成以下两个工作

1.程序运行栈的初始化。程序的运行离不开栈，程序运行时所需的临时变量、数据、函数间调用的参数传递等都需要栈的支持，操作系统在启动时，必须首先解决好堆栈的初始化问题。

2.处理器及外设的初始化，中断处理程序能够提供给操作系统非顺序的、突发的执行逻辑，时钟设备则可以产生固定频率的时钟驱动整个系统运行，所以在操作系统各功能模块得以发挥作用之前，处理器及外设也必须被初始化。

### 二、操作系统初始化代码

本章的操作系统初始化代码示例包括start.s init.s abnormal.s boot.c 四个文件

#### (一) start.s
```
.section .startup
.code 32
.align 0

.global _start                ;全局变量
.extern __vector_reset        ;引用外部变量
.extern __vector_undefined
.extern __vector_swi
.extern __vector_prefetch_abort
.extern __vector_data_abort
.extern __vector_reserved
.extern __vector_irq
.extern __vector_fiq

_start:
	ldr pc,_vector_reset
	ldr pc,_vector_undefined
	ldr pc,_vector_swi
	ldr pc,_vector_prefetch_abort
	ldr pc,_vector_data_abort
	ldr pc,_vector_reserved
	ldr pc,_vector_irq
	ldr pc,_vector_fiq

.align 4

_vector_reset:			.word  __vector_reset        ;定义变量_vector_reset，并用__vector_reset赋值        
_vector_undefined:		.word  __vector_undefined
_vector_swi:			.word  __vector_swi
_vector_prefetch_abort:	.word  __vector_prefetch_abort
_vector_data_abort:		.word  __vector_data_abort
_vector_reserved:		.word  __vector_reserved
_vector_irq:			.word  __vector_irq
_vector_fiq:			.word  __vector_fiq
```
#### (二) init.s

```
.equ DISABLE_IRQ,	0x80                       ;定义宏
.equ DISABLE_FIQ,	0x40
.equ SYS_MOD,		0x1f
.equ IRQ_MOD,		0x12
.equ FIQ_MOD,		0x11
.equ SVC_MOD,		0x13
.equ ABT_MOD,		0x17
.equ UND_MOD,		0x1b

.equ MEM_SIZE, 		0x800000
.equ TEXT_BASE,		0x30000000
.equ _SVC_STACK,	(TEXT_BASE+MEM_SIZE-4)
.equ _IRQ_STACK,	(_SVC_STACK-0x400)
.equ _FIQ_STACK,	(_IRQ_STACK-0x400)
.equ _ABT_STACK,	(_FIQ_STACK-0x400)
.equ _UND_STACK,	(_ABT_STACK-0x400)
.equ _SYS_STACK,	(_UND_STACK-0x400)

.text
.code 32
.global __vector_reset

.extern plat_boot                                ;外部函数
.extern __bss_start__                            ;在链接脚本中赋值
.extern __bss_end__

__vector_reset:                                  ;设置每种cpu工作模式下的堆栈空间
	msr cpsr_c,#(DISABLE_IRQ|DISABLE_FIQ|SVC_MOD)
	ldr sp,=_SVC_STACK
	msr cpsr_c,#(DISABLE_IRQ|DISABLE_FIQ|IRQ_MOD)
	ldr sp,=_IRQ_STACK
	msr cpsr_c,#(DISABLE_IRQ|DISABLE_FIQ|FIQ_MOD)
	ldr sp,=_FIQ_STACK
	msr cpsr_c,#(DISABLE_IRQ|DISABLE_FIQ|ABT_MOD)
	ldr sp,=_ABT_STACK
	msr cpsr_c,#(DISABLE_IRQ|DISABLE_FIQ|UND_MOD)
	ldr sp,=_UND_STACK
	msr cpsr_c,#(DISABLE_IRQ|DISABLE_FIQ|SYS_MOD)
	ldr sp,=_SYS_STACK

_clear_bss:                                    ;清空bss
	ldr r1,_bss_start_
	ldr r3,_bss_end_
	mov r2,#0x0
1:
	cmp r1,r3
	beq _main
	str r2,[r1],#0x4                        ;将r2的0写到r1存放的地址，并将r1中地址增加4
	b 1b                                    ;跳转到标号1继续执行，这里相对于是个循环清零，直到r1==r3，表示清到_bss_end_

_main:
	b plat_boot                        ;b所能跳转的最大范围是当前指令地址前后的32M，因为操作数长度为24bit，因此还可以用mov，ldr指令对pc寄存器赋值达到跳转目的


_bss_start_:	        .word  __bss_start__
_bss_end_:		.word  __bss_end__

.end
```

#### (三) abnormal.s

异常暂时还没有处理
```
.global __vector_undefined
.global __vector_swi
.global __vector_prefetch_abort
.global __vector_data_abort
.global __vector_reserved
.global __vector_irq
.global __vector_fiq

.text
.code 32

__vector_undefined:
	nop
__vector_swi:
	nop
__vector_prefetch_abort:	
	nop
__vector_data_abort:
	nop
__vector_reserved:
	nop
__vector_irq:
	nop
__vector_fiq:
	nop

```
#### (四) boot.c

```
typedef void (*init_func)(void);

#define UFCON0	((volatile unsigned int *)(0x50000020))

void helloworld(void)
{
	const char *p = "3.1_helloworld\n";
	while(*p) {
		*UFCON0 = *p++;
	};
}

static init_func init[] = {
	helloworld,
	0,
};

void plat_boot(void)
{
	int i;
	for(i = 0; init[i]; i++) {
		init[i]();
	}
	//while(1);
}
```

#### (五) skyeye配置

由于本章将内存地址设置为0x30000000，而新版skyeye加载bin文件的load-file命令不能在加载结束后设置pc指针到指定地址，因此使用load-binary指令直接加载elf文件
吐个槽，新版的命令有点复杂，不如以前skyeye.conf简单明了
```
define-conf skyeye.json
load-binary  s3c2410x1_core_0 leeos.elf
init-ok
```


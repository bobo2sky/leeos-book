# leeos-book

#### 介绍
一步一步编写操作系统（skyeye新版实验）

[基于WSL2的Skyeye环境](https://open-skyeye.gitee.io/wiki/#/docs/zh-cn/community/share/wsl2/text)

[快速开始](https://open-skyeye.gitee.io/wiki/#/docs/zh-cn/quick-start/text)

#### 目录

[2.1 使用c语言写第一段程序](2.1_使用c语言写第一段程序/text.md)

[2.2 用脚本链接目标文件](2.2_用脚本链接目标文件/text.md)


#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
